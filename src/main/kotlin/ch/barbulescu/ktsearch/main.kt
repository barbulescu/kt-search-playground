package ch.barbulescu.ktsearch

import com.jillesvangurp.ktsearch.*
import com.jillesvangurp.ktsearch.Refresh.WaitFor
import com.jillesvangurp.searchdsls.SearchEngineVariant.ES7
import com.jillesvangurp.searchdsls.SearchEngineVariant.ES8
import com.jillesvangurp.searchdsls.querydsl.matchAll
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable

const val indexName = "student"

fun main(): Unit = runBlocking {
    SearchClient(KtorRestClient(host = "localhost", port = 9999)).use { client ->
        println(client.engineInfo())
        println(client.clusterHealth())
        client.validateEngine("Supports only ES7 and ES*", ES8, ES7)

        client.deleteIndex(indexName, ignoreUnavailable = true)
        client.createStudentIndex()
        client.indexDocument(indexName, document = Student("Marius", 12), refresh = WaitFor)
        client.search(indexName)
            .parseHits<Student>()
            .forEach(::println)

        client.deleteByQuery(indexName) {
            query = matchAll()
        }
    }
}

private suspend fun SearchClient.createStudentIndex() {
    if (exists(indexName)) {
        return
    }

    createIndex(indexName) {
        settings {
            replicas = 0
            shards = 3
        }
        mappings(dynamicEnabled = false) {
            keyword(Student::name)
            number<Int>(Student::age)
        }
    }
}

@Serializable
data class Student(
    val name: String,
    val age: Int
)